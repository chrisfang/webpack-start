import React from 'react';
import ReactDOM from 'react-dom';
import './app.scss';

class App extends React.Component{ 
	constructor(props){
		super(props);
		this.state = {
			name: 'chris'
		}
	}

	render(){
		const { name } = this.state;
		return(
			<section className="chris">{name}</section>
			);
	}

} 

ReactDOM.render(<App/>, document.getElementById('app'));
'use strict';

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsWebpackPlugin = require('optimize-css-assets-webpack-plugin');

const glob = require('glob');
const setMap = () => {
	const entry = {};
	const htmlWebpackPlugins = [];
	const entryFiles = glob.sync(path.join(__dirname, './src/*/index.js'))
	console.log(entryFiles);
    Object.keys(entryFiles)
		.map((index) => {
			const entryFile = entryFiles[index];
            const match = entryFile.match(/src\/(.*)\/index\.js/)
            const pageName = match && match[1];
            entry[pageName] = entryFile;
            htmlWebpackPlugins.push(
                new HtmlWebpackPlugin({ // html 压缩
                    template: path.join(__dirname, `src/${pageName}/index.html`),
                    filename: `${pageName}.html`,
                    chunks: ['commons', pageName],
                    inject: true,
                    minify: {
                        html5: true,
                        collapseWhitespace: true,
                        preserveLineBreaks: false,
                        minifyCSS: true,
                        minifyJS: true,
                        removeComments: false
                    }
                })
            )
		})

		return {
			entry,
			htmlWebpackPlugins
		}

};

const { entry, htmlWebpackPlugins } = setMap()
console.log(entry);
module.exports = {
	mode: 'none',
	entry: entry,
	output: {
 		path: path.join(__dirname, 'dist'),
  		filename: '[name]_[chunkhash:8].js'
	},
	module: {
		rules: [
			{
				test: /.js$/,
				use: 'babel-loader'
			},
			{
				test: /.scss$/,
				use:[MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader',{
					loader: 'postcss-loader',
					options: {
						plucins: () =>{
  							require('autoprefixer')({
                                overrideBrowserslist: ['last 2 version', '>1%', 'ios 7']
                            })
						}
					}
				},
				 {
                    loader: 'px2rem-loader', // 将css中的px转为rem 搭配lib-flexible（自动计算根元素的font-size）
                    options: {
                        remUnit: 75,
                        remPrecision: 8
                    }
                }]
			}
		]
	},
	plugins: [     
		new MiniCssExtractPlugin({ // css hash
            filename: '[name]_[contenthash:8].css'
        }),
        new OptimizeCssAssetsWebpackPlugin({ // css压缩
            assetNameRegExp: /\.css$/g,
            cssProcessor: require('cssnano')
        })
        , new CleanWebpackPlugin() 
        ].concat(htmlWebpackPlugins)

}	
